package top.chengdongqing.common.interceptor;

import java.util.stream.Stream;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

import top.chengdongqing.common.annotation.NotBlank;
import top.chengdongqing.common.kit.Fail;

/**
 * 参数非空拦截器
 * 
 * @author Luyao
 *
 */
public class NotBlankInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		// 判断该方法是否添加了该注解
		NotBlank params = inv.getMethod().getAnnotation(NotBlank.class);
		// 判断该注解是否有参数
		if (params != null && params.value().length > 0) {
			Controller c = inv.getController();
			// 循环判断需要校验的参数是否为空
			Stream.of(params.value()).forEach(key -> {
				if (StrKit.isBlank(c.get(key))) {
					c.renderJson(Fail.setMsg(Fail.Msg.PARAM_EMPTY));
					return;
				}
			});
		}
		inv.invoke();
	}
}
