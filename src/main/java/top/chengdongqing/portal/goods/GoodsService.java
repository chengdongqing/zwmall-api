package top.chengdongqing.portal.goods;

import java.math.BigDecimal;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

import top.chengdongqing.common.config.Constant;
import top.chengdongqing.common.config.Constant.Status;
import top.chengdongqing.common.model.Goods;
import top.chengdongqing.common.model.GoodsSku;
import top.chengdongqing.portal.goods.bo.SkuDetail;

/**
 * 商品相关业务层
 * 
 * @author Luyao
 *
 */
public class GoodsService {

	@Inject
	Goods dao;

	@Inject
	GoodsSku skuDao;

	/**
	 * 查询最新的10个商品
	 * 
	 * @return
	 */
	public List<Goods> findHotGoodsList() {
		String sql = "select id, name from goods where status = ? order by createTime desc limit 10";
		return dao.find(sql, Status.ENABLED);
	}

	/**
	 * 获取搜索提示词
	 * 
	 * @param keyword
	 * @return
	 */
	public List<String> findPromptKeywords(String keyword) {
		String sql = "select name from goods where name like ? and status = ? order by createTime desc limit 10";
		return Db.query(sql, "%" + keyword + "%", Status.ENABLED);
	}

	/**
	 * 商品搜索
	 * 
	 * @param keyword
	 * @param categoryId
	 * @param brandId
	 * @param sort
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Goods> searchGoodsList(String keyword, int categoryId, int brandId, String sort, int pageNumber,
			int pageSize) {
		// 搜索商品列表
		Kv params = Kv.by("keyword", keyword).set("categoryId", categoryId);
		params.set("brandId", brandId).set("sort", sort).set("status", Status.ENABLED);
		return dao.template("goods.search", params).paginate(pageNumber, pageSize);
	}

	/**
	 * 获取随机推荐的商品
	 * 
	 * @return
	 */
	public List<Goods> findRecommends() {
		return dao.template("goods.findRecommends", Status.ENABLED).find();
	}

	/**
	 * 查询商品详情
	 * 
	 * @param id
	 * @return
	 */
	public Goods findDetails(int id) {
		return CacheKit.get("goodsDetails", id, () -> {
			Goods goods = dao.findByIdLoadColumns(id, "id, name, sketchType, sketch, specs, status");
			if (goods == null || goods.getStatus() == Constant.Status.DISABLED) {
				return null;
			}
			return goods;
		});
	}

	/**
	 * 获取商品规格
	 * 
	 * @param id
	 * @return
	 */
	public Ret findSkus(int id) {
		return CacheKit.get("skus", id, () -> {
			Goods goods = dao.findByIdLoadColumns(id, "id, name, introduction, maxBuy, skuType");
			if (goods == null) {
				return Ret.fail(Constant.MSG, "该商品不存在");
			}

			// 查询该商品所有的规格类别
			List<GoodsSku> skus = skuDao.template("goods.findSkus", id).find();
			if (skus.size() == 0) {
				return Ret.fail(Constant.MSG, "获取商品信息失败");
			}

			// 获取每个类别下的子项
			skus.forEach(sku -> {
				sku.put("sellout", sku.getStock() <= 0);
				sku.remove("stock");
			});
			return Ret.ok("goods", goods).set("skus", skus);
		});
	}

	/**
	 * 根据商品id和规格id查询商品规格详情
	 * 
	 * @param goodsId
	 * @param skuId
	 * @return
	 */
	public SkuDetail getSkuInfo(int goodsId, int skuId) {
		return CacheKit.get("sku", skuId, () -> {
			// 查询该规格详情
			Goods sku = dao.template("goods.skuInfo", goodsId, Status.ENABLED, skuId).findFirst();
			if (sku == null) {
				return null;
			}

			// 获取商品图片
			String thumbUrl = sku.getStr("bannerUrls").split(",")[0];
			// 获取商品全称
			StringBuilder name = new StringBuilder(sku.getStr("name"));
			if (sku.hasVariousSpec()) {
				name.append(" ").append(sku.getStr("skuName"));
			}
			// 获取其余信息
			BigDecimal price = sku.getBigDecimal("price");
			BigDecimal originalPrice = sku.getBigDecimal("originalPrice");
			int stock = sku.getInt("stock");
			int maxBuy = sku.getInt("maxBuy");

			// 封装商品信息
			return new SkuDetail(goodsId, skuId, name.toString(), thumbUrl, price, originalPrice, stock, maxBuy);
		});
	}
}
